<?php
include 'config.php';

    // ADD
    if(isset($_POST['username'])  AND isset($_POST['password'])){
        
        $user = $_POST['username'];
        $pass = $_POST['password'];
      
        $hashed_string['username'] = $user;
        $hashed_string['password'] = $pass;
        
        $data_post = array(
            'data' => $hashed_string,
        );
   
		$response = get_content($titu.'/api/v1/login', json_encode($data_post));
        
        unset($_POST['username']);
        unset($_POST['password']);
        
        $response = json_decode($response);
        
		if(isset($response->status->error->message)){
            
		}else{
			
			if($response->data->userRole <> 1){

				header('Location: ' ."login.php?error=No Access");
				exit;
			}
			
			$_SESSION['user']['UserName']   = $response->data->userName;
			$_SESSION['user']['FullName']   = $response->data->userFullName;
            $_SESSION['user']['Role']       = "Agent Intelligence";// $row['authRole'];
            
            header("Location: dashboard");
            exit;
		}
		
    }

?>
