<?php
include 'xyz/config.php';

if(!isset($_SESSION['user'])){
    require __DIR__ ."/ui/login.php";
    exit;   
}

$request = $_SERVER['REQUEST_URI'];

$request = explode("/",$request);

$params = null;
if(isset($request[2])){
	$params = $request[2];
}else{
	$params = $request[1];
}

$params = explode("?",$params);

if(isset($params[1])){
    $_gets = $params[1];
    $_gets = explode("&",$_gets);
    foreach($_gets as $vall ){
        $vall = explode("=",$vall);
        if(count($vall) == 2){
           $_get[$vall[0]] =  $vall[1];
        }
    }
}

$params = $params[0];

switch ($params) {
    case '' :
        header("Location: xyz/transaction");
        break;
	case 'dashboard' :
        require __DIR__ . '/ui/transactionpayables.php';
        break;
    case 'transaction' :
        if(isset($request[3]) AND $request[3] != ""){            
            require __DIR__ . '/ui/invoice.php';
            break;
        }
        require __DIR__ . '/ui/transaction.php';
        break;
	case 'transactioninvoice' :
        require __DIR__ . '/ui/transactioninvoice.php';
        break;
	case 'transactionbni' :
        require __DIR__ . '/ui/transactionbni.php';
        break;
	case 'settlementmidtrans' :
        require __DIR__ . '/ui/settlementmidtrans.php';
        break;
	case 'settlementbni' :
        require __DIR__ . '/ui/settlementbni.php';
        break;
	case 'transactionmidtrans' :
        require __DIR__ . '/ui/transactionmidtrans.php';
        break;
	case 'transactionpayables' :
        require __DIR__ . '/ui/transactionpayables.php';
        break;
	
    default:
        break;
}


function change_event($event) {
    if((int)$event <> 0 ) {
        foreach($_SESSION['user']['Events'] as $vall){
            if($vall->evnhId == $event){
                $_SESSION['user']['Event']      = $vall->evnhId;
                $_SESSION['user']['EventName']  = $vall->evnhName;
            }
        }
    }
}

?>